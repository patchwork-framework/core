# Patchwork Core

Core package of Patchwork - an asynchronous distributed microframework.

This library contains some core structures and utils to re-use over all Patchwork
packages.


## Requirements
* Python 3.7

## Contribution notes

.. warning::
    Remember to run `compile-protoc` script after every change in proto files from `protocol` directory. 
    Make sure that all these changes follow guidelines described in Google Protobuf v3 documentation.

## Installation

``pip install patchwork-core``

## Documentation

https://patchwork-framework.gitlab.io/core

