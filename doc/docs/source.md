# Patchwork Core

Project source code can be found at [GitLab](https://gitlab.com/pawelpecio/patchwork/core).

Package name: `patchwork-core`

This package contains a core files of Patchwork Microframework. Core has been extracted to a separate package to allow
building thin packages which works in Patchwork ecosystem. One of the goal for Core is to be compatible with
all major Python versions, also 2.7, in opposite to Patchwork Node or Gateway which are designed to be fully
asynchronous and use a lot of new Python features which came in version 3.7.

## Component
::: patchwork.core.component

## Client
::: patchwork.core.client.subscriber
::: patchwork.core.client.publisher
::: patchwork.core.client.local

## Config
::: patchwork.core.config.base

## Misc
::: patchwork.core.typing
::: patchwork.core.utils