# Welcome to Patchwork Core

[![PyPI version](https://badge.fury.io/py/patchwork-core.svg)](https://badge.fury.io/py/patchwork-core)
[![MIT](https://img.shields.io/badge/license-MIT-blue)](license.md)

Patchwork is a simple framework for asynchronous microsystems ecosystem. It's like web services working with message
brokers instead of HTTP/GRPC.

This framework is under development. Current version is working stable, Core API is ready and shouldn't change very much.
Kafka client and node are operational and working on production environment, however due to my limited time and resources
it's not fully tested and documentation is not up-to-date.

If you'd like to contribute, improve or have some thoughts, join at 
[Gitlab](https://gitlab.com/patchwork-framework/core).

!!! warning
    As of version 0.2 framework requires Pydantic 2.0

## Why to use, how it works?

* **simple**, only must-have functionality in main packages, more available in specialized ones
* **asynchronous**, 100% async operation at architecture, layout and implementation\
* **easy to integrate**, run as a standalone service with Docker/K8S, integrate with your code or other async python framework
* **stable**, at least one delivery, ACKs, retries and backoff queues

## Installing

The Core package is not operational itself, it's a base package for whole framework with common code for both
producers and workers. You bootstrap your own component using the Core or install one of ready to use producers,
workers or services:

* `patchwork-kafka-client` ([![PyPI version](https://badge.fury.io/py/patchwork-client-kafka.svg)](https://badge.fury.io/py/patchwork-client-kafka)), the producer to push messages using Apache Kafka
* `patchwork-node` ([![PyPI version](https://badge.fury.io/py/patchwork-node.svg)](https://badge.fury.io/py/patchwork-node)), worker implementation to integrate or use standalone
* `patchwork-websoccer` ([![PyPI version](https://badge.fury.io/py/patchwork-websoccer.svg)](https://badge.fury.io/py/patchwork-websoccer)), gateway to forward messages between internal communication broker and external World using websockets

To install just a core run:

`pip install patchwork-core`
