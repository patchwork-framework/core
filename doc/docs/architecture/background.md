# Background
Nowadays many complex applications are implemented using microservices. Data models
and functionalities are splitted into many services which should be independent. In practice,
this 'independece' is not so easy to achieve. Moreover when application grows and new
functionalities are appended it becomes hard to plan and maintain internal communication
between services. In most cases it's done by some kind of API and direct service to service
communication. For example, user want to register on our platform and fill registration form
which is send to API Gateway service as REST HTTP request, but user registration is not
just adding an entry to users table in database. On complex platform a lot of things must be
done for proper user account initialization. It becomes more complex if we have many
user account types or initialization needs cooperation with 3rd parties or our new user.
One day we discover that our registration microservice which handles such requests became
really huge and maintain user validation process, sending confirmation emails and 
resources initialization. But *eheu!* we are good architecture designers and our service
just makes many requests to other microservices to initialize resources for given user.
Sounds like a good solution, but... what happens if something goes wrong? One of
microservice was down during initialization and was not able to take expected action.
In meanwhile, our system has been intergrated with other one and we need to transfer users.
It looks like there is not one, even not two but about five or more ways to create
new user account and registration microservice is a bottleneck. It would be great
to split it and build dedicated service to handle each registration flow, but this solution
introduce requests-response mess in cross-services communication. Any API change is hard
to do and hard to deploy. It becomes hard to do retry actions if one kind of services
are down, difficult to scale and monitor. By the way... our user is still waiting for
the server response, request is going through ten services and each one needs to confirm
that operation was successful before API Gateway returns ++"200 OK"++ response.

To address such cases some applications introduced messages queue. There is no direct
communication between services which have no HTTP API proxied by API Gateway. Each service
is independent component listening on certain queues and waiting for messages. If more
actions is need on user registration - no problem, we can deploy new component listening
on appropriate queue for `newUserRequest` events. However, there is one issue - how to tell
our user who waits for ++"200 OK"++ response that everything goes OK and account has been
created successfully? Yeah... that's problem. We don't know exactly how many services
must take action on user registration, we don't want to know.

!!! note
    Remember this rule: **we don't know and don't want to know**.
    
    If we want, we need to code 
    this somewhere and change this code each time new component related to registration process
    will be deployed which is against our approach

But wait a minute, why user is waiting for ++"200 OK"++ response? Does really user needs to know that his account has been created just
after this account has been created? Usually application send confirmation email. In fact,
in most cases (not only registration process) user is not expecting update, create or
delete action result *immediately*. It's OK if we tell that request has been accepted.
Only data retrieval are expected to happen immediately. I want to see something now, but
usually it's OK if I publish new content and it become visible after a few seconds.
However, such architecture is sensitive for design
mistakes. Each component must be created carefully to avoid data loss. Application can't tell
the user that something will be published and later doesn't publish it because one
of services had a bad day and crashed.

!!! example
    This approach is taken from network games. Client application is sending status update
    requests, but is not waiting for the result. Result will be received with one of subsequent
    game frame events which updates game state. So if gamer want to move one step forward,
    application sends request: `move player A one step forward` and doesn't wait for the result
    with new player position. Gamer can take another action in the meanwhile (which is fact
    very short, around 100 ms, but he can). 
    
    After some time server sends updated game state with
    new player A position which reflects requested one step forward move. In the same way
    complex web applications can work. 

This framework has all these things in mind. It tend to be a skeleton for components 
(services) written in Python. The goal is to have a framework which handles all common
tasks for such asynchronous services. New service implementation should be limited to just one
method which do actual business logic and subscription on given queue for given messages type.

In final solution framework will provide a mechanism to monitor components, scale them,
do blue-green deployments and guarantee no data loss at component level. Just to clarify,
here we talk about mechanism in components which allows to, framework of course can't
provide all these things alone.  