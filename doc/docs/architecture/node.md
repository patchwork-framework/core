# Node architecture

Node is a single component. Can be deployed separately and should be responsible of 
one well defined set of actions or resource. Nodes can be scaled and it's allowed
(and recommended) to have multiple instances of the same component working at the same
time. 

Technically node is a Python process which consumes messages, executes processors with
some additional monitoring code:

* `Worker` root-class which implements entry point
* `Executor` which listens on clients for incoming messages and run them on processing unit
* `ProcessingUnit`, which is a part responsible of running given task
* `Manager`, which allows to get state of the node and maintain it
* `Coordinator` (planned), which is a module responsible of communicating with other
component instances and getting a consensus in resource id routing

Each module (except `Worker`) can be replaced by other implementation provided by another
library with just a configuration change.

!!! hint
    Patchwork node is available also as docker container. If you don't need to extend
    or override node code it's possible to just create your own docker image with
    processor code placed in root directory. Patchwork node will run at container
    startup, discover processors and setup.