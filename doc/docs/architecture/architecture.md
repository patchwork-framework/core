# Principles 

### Assumptions

* message broker is transactional (allow to get message and commit or rollback it later)
* message broker can retry undelivered or uncommitted messages
* one data model is maintained by only one component, however it's allowed to have
  multiple component instances which should still work without collision
* messages format must be at least one version backward compatible
* every component may goes down at any time without data loss
* every message can be duplicated or delivered multiple time to the same
  component which should not take same action twice
* no direct communication between components (except data retrieval)
* all CUD (create, update and delete) actions are done by events (messages)
* multiple components may subscribe for the same events, multiple components may
  produce same type of events
* every action is asynchronous, there is no blocking waits for other components or
  external systems (eg database)
  
### Message broker is transactional
To achieve no data loss (or very low in less critical applications), message transport must
guarantee that message won't be lost if consumer goes down between taking the message from 
the queue and producing the result. We can guarantee that by get-commit-rollback mechanism.
Message broker should consider message as delivered and remove it from the queue when
all consumer commits that has read it. Alternatively, the broker may track read offsets
and remove messages when retention time expires (like Apache Kafka). 

Such approach is named *at least one delivery* guarantee and
this framework has been designed and implemented with this assumption. 
One of disadvantage is that one message can be delivered more than once, but at opposite we have *at 
most one delivery* guarantee which assumes that there are cases when messages can be lost.

In fact it's not complicated and in many real use cases
it's quite easy to make sure that duplicated messages does not cause any side-effects.
For instance instead of just DB `INSERT` queries `CREATE OR UPDATE` can be done with
extra `eventID` column which can be lookup'd in such query. Locally component may track
last `n` messages IDs. Some brokers keeps messages ordered, so if message is retried all
attempts happens one by one - such ID track list won't be too long.  

!!! warning
    It's near to impossible implement *once and only once delivery guarantee*. 
    
    Do-able are less strict guarantees:
    
    * *at least one delivery*
    * *at most one delivery*
    
    In first case it's guaranteed that message has been delivered, but in some cases
    it's expected that message can be delivered more than once. If guarantee is
    satisfied using transactions such situation may happen when consumer/client
    was considered as failed between receiving message and commiting them. Message
    broker may consider client as died, queue message to be re-send but before do that
    client reconnects and receives same message again.
    
    In the second case message is removed from the queue when delivered without
    waiting for commit. So it's guaranteed that message won't be delivered more than
    once, but it's possible that if consumer/client goes down just after receiving message
    expected action never happens. 

### Message broker can retry
Message broker should automatically retry undelivered messages or keep them on the queue
as long as it's needed. As we our application is dynamic and in fact we don't know and 
don't want to know how many consumers should read it, it could be great idea to
setup time retency on the queue. In most cases a few hours should be enough, which is
depending on message size (which should be low) just a few GBs on high-traffic queues. 

!!! hint
    By the way, it's a good practice to keep messages size small. Send all big data as
    *attachments* stored on the separate fast storage. Most message brokers really
    don't like messages larger than 1MB, usually recommends a few kilobytes.

Remember to keep your retention time enough to take an action if something starts going
wrong. It would be great if message broker can increase retention time on demand. 
With such broker default retention can be low and if monitoring service find any 
anomalies it's increases this time.

### One data model is owned and maintained by one component
Our application is asynchronous and not strictly defined. If two different components
can manipulate same data it's a straight highway to conflicts, which leads to locks and
synchronization mechanism which is therefore straight way to performance disaster.
Don't do this! One data model **have to** be maintained only by one component! 
Writing the component keep in mind that there could be more than one instance
working on the same time and two instances *may* process same duplicated event.
Even if not, two events may refer to the same resource. However, along same component
instances working on the same codebase it's do-able to queue resource changes or
implement quite effective routing mechanism which ensures that events related to the
same resource ID always goes to the same component instance.

### Backward compatible messages format
**It's not possible to update multiple components at the same time**. Remember
that. Not possible. And not recommended. So, if producer starts producing new messages
make sure, that new format won't break any existing consumers on their current versions.
Also, it's a really good practice to do blue-green deployments and have two component
versions at the same time. So having at least two versions of message format mixed
on the same queue is a **normal** situation which must be supported. 

How to do that? Make messages protocol not strict, keep in mind that there might more 
fields in the future. It's a good idea to add version number. If you need fast messages
serialization and don't want to use rich formats like JSON/BSON, consider messagepack
which is quite fast and still flexible with dynamic structures support. If not (on Python
I really can't imagine why) and you have no choice and must use fixed structures, all
changes introduce as additive. Prepare consumers that message may be longer then
expected, but skip unknown rest of the payload. All changes do as adding new fields, even
if you just change existing field format. Yes, message becomes longer and longer with
every change, but at some day when some fields becomes obsolete and not used by any
component for a long time it can be marked as reserved bytes and reused. But again,
on Python with it's speed and memory overhead - I can't image a case when fixed structure 
must be used because dynamic one is a performance or memory bottleneck...

### Component fault-tolerance
At any time someone in datacenter may just push your server power button... so
your component just received message and started processing or processed it, done
what should be done and was preparing commit request when power lost. *At any time*
means really at any time and... in any way. It does not have to be a power button,
it was a network cable. So, component took message, processed it but was unable to commit.
Message broker discovered that component instance was lost, no heartbeats, no commits,
so delivers same message to another instance which is working on different server.
This component also do what should be done and commit successfuly. Then, someone
in datacenter found that all these red alerts is just a network cable cut by
a mouse and plug new cable. Our primary component instance becomes alive and resumes
by committing already committed message. There should be still no side effects!
Messages routing mentioned in {ref:} is one of the easiest solution. Each component
instance is working with well-defined set of resource ids. If one of instance became
unavailable, others renegotiates routing rules and old one should be scheduled to be
killed. On the opposite side, if there are commits errors and there is a timeout after
which instance is marked as failed, at the same timeout orphaned instance should 
stop trying to commit and probably just go down itself. It doesn't matter what was done
and is pending to be committed, no one is waiting for this. Life is brutal and full of traps.

### No direct communication - only events
As **we don't know and don't what to know** rule, there is no application architecture
defined upfront. There is no guarantee that something which is for sure really, really
internal action which for sure will be done only between these two components will be
still so internal after tomorrow meeting with the PM. No direct communication at all.
No off-side requests. Every change in application state must be done though event.
Anyone at anytime might be interested in listening for this change. No more *for sure,
ever, really, really never...* The only thing which can be obtained directly, and should be,
is current application state. So feel free to expose retrieval API, never expose
update API.

By the way... did you hear about caching? of course yes. Use caches. Less retrieval 
requests, less network traffic, less electrons, less CO2. **Be eco, use caches**. Wait,
but how long I should cache data? Forever - as long as you need, but use your consumer
to listen for resource update events and remove cached resource when owner component
notifies update.

!!! hint
    Really, never consider that something is unchangeable, internal and permanent. 
    
    *But in this world nothing can be said to be certain, except **death** and **taxes***
    /Benjamin Franklin/

### Many to many queues
Event is not owned by any component. Resource and functionalities are. So it's really OK
that there are events which are not produced by any components or there are events which
are not consumed by anyone. Moreover, as there is no owner, so anyone can produce any
kind of message and anyone can listen for any kind of messages. Event messages must be
well defined and documented, but to be fully flexible any component can implement
support for any message at any time. However, keep in mind that listening for all
events isn't a good idea. There will be a day, when message format must change. Of
course new format will be backward compatible, but too many backward compatible changes
are hard to maintain. Less components use same message - less components to change
on message format update. There is also one more conclusion: do not declare too wide
meaning messages. Messages should be granular. One message type with a dynamic dictionary,
send by all components and consumed by all components is not a good idea. At the opposite
side, one message type for any single change with hundreds of message types and components
subscribing for many different messages is also not a good idea. Something on the middle 
should be a good choice. Where exactly? It depends on use case. Usually one component
should react to no more than a few messages, at the beginning three message types sounds
reasonable: create, update, delete resource. Like in old, good REST CRUD approach (do you remember that retrieve
is not an event?). 

### Everything is asynchronous
Nowadays waiting for something on blocking requests is really old-school. As Python 
has stable asyncio support, whole framework is done in async fashion. No blocking.
At architecture level also everything must be asynchronous. When event is send, there
is no way to tell how many components react because **we don't know that and don't 
want to know**. So it's hard to tell for what code should wait. For a notification from
the resource owner? Maybe, but even if user has been stored in database it's not guaranteed
that confirmation email has been sent, which is a responsibility of another component.
So returning a ++"200 OK"++ response when user has been stored but confirmation email was not
send and maybe never will be is not a good choice. From user perspective it doesn't matter
if everything goes successfully except confirmation email. User doesn't know that account
has been created and everything goes successfully until receive confirmation email.
Conclusion is that it makes no sense to wait for action result. 

Wait only for what have to,
like confirmation from message broker that event has been stored (but don't wait
for delivery ACK). Even with simple web applications asynchronous is not a problem.
With current wide usage of websockets, supported by all major frontend frameworks
it's really not a problem to send filled form in POST request and return response
immediately to the browser after form validation and storing appropriate message on the
queue. Other service which handles user webpage state updates listens for changes
and notify user later when requested action will be done. In meanwhile user may start
doing something else. If for some reason user has to wait - no problem. Show a loading
indicator until state update arrives though websocket. Adding some kind of correlation id
in state update requests and state changes notifications will be helpful, but think 
twice if user really must wait and watch your beautiful loader. In really most cases - not.

!!! hint
    Do you remember games? When you shot to your counterparty game is not blocking. You can start
    running away to avoid counterstrike. So if games don't have to freeze on shot action in web 
    application user also doesn't have to wait and watch spinning grey dots.