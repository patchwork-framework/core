# Overview

Client packages add support for 3rd party message brokers. Patchwork node itself
is not supporting any broker by default, instead is expecting external packages to
support defined interface.

Outside of Patchwork node clients can be used to send new task from existing applications
without installing whole Patchwork framework. Clients are small and in opposite to the
framework might support legacy Python versions, even Python 2.7.

Both, synchronous and asynchronous clients have the same class interface, the difference
is that async client defines coroutines instead of regular methods. To determine
client type `is_asynchronous` flag is checked.  

### Synchronous client

Client package may implement synchronous client which integrates very easily with
existing synchronous applications. All operations are blocking and returns when
completed (or failed).

!!! warning
    Synchronous clients cannot be used in Patchwork nodes.

### Asynchronous client

Client package should implement asynchronous client and all interface methods as
coroutines. There should be no blocking operations. If external transport library
does not support async use Python ThreadPoolWorker to run blocking code on a
separate thread. See Python async documentation for more details.

!!! hint
    If your client package tends to support Python versions prior to async, make sure
    that whole async code is not reachable by older Python interpreters.
    Using `if` statement with `__version__` check could be a good choice.
    
## Setting up client

To start working with client it must be instantiated. All clients have at least following
init arguments:

* `name`, which is a client human readable name, used for instance in logger
* `serializer`, which is a module, class or class instance which has `loads()` and `dumps()`
    method defined

!!! note
    Depending on message broker supported by the client, there could be more
    required arguments in `__init__`. Consult client documentation for more details.
    
Instantiated client is not ready to use, it must be started. `start()` method must
be called before first client usage and is designed to setup connection with
supported broker. Once client is started it can be used to fetch or send tasks.

When client is no longer needed or application is closing client should be finalized
using `stop()` method. 

!!! danger
    Only finalized client should be destroyed to make sure that there will be no
    data loss. It's allowed in client implementation to cache data or perform
    lazy commits which must be flushed on stop.

!!! note
    For asynchronous clients `start()` and `stop()` methods are coroutines and
    should be awaited.

### Task serialization

To send task over the wire it must be serialized to binary data.
Serialization can be done by any class or module which supports `dumps()` and `loads()`
methods which makes expected interface fully compatible with Python standard 
serialization libraries like `json` package.

As an argument task serializer gets task state which has only Python standard types
used inside. No support for custom classes is needed, so any serializer which follows
standard `dumps()` signature should work out of the box. Kind of output produced by
serializer depends on client transport expectations. Consult client documentation for
more details.

Task deserialization is done by `loads()` methods which should takes same data type
as `dumps()` produce and return task state in the same format as given to serialization
method.

Recommended serialization libraries are:

* [MessagePack](https://msgpack.org/) which supports dynamic data structures like dict
    and lists, is fast, has little memory and performance overhead. The big disadvantage
    is that output is binary and not human readable.
* [JSON](https://docs.python.org/3/library/json.html), which also supports dynamic
    data structures but is slower and has more overhead than message pack. In opposite
    JSON produces human readable output so if transport
    software allows to see messages debugging might be easier.


!!! note
    Conversion from task class instance to state and vice versa is an internal job
    of task class implemented in `__getstate__()` and `__setstate__()` methods. 
    However, as not all serialization libraries support these methods out of the box,
    client code calls them explicitly and pass result to serializers.

!!! danger
    Using `pickle` as serialization class is highly discouraged as it's not safe and
    malformed messages may lead to unexpected remote code execution.
    
    See [`pickle` security considerations](https://docs.python.org/3/library/pickle.html)
    for more details. 