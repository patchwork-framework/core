# Writing custom clients

As described in [Overview](overview.md) chapter, client is just a class with expected interface
implemented. To write custom client inherit from `SynchronousClient` or `AsyncClient`
classes from `patchwork-client-base` package. It's also possible to write clients
from scratch, but base package provides some core functionality which is common
for all clients and should be a good point to start.

There are four methods which must be implemented:

* `_send()`
* `_fetch_one()`
* `_commit()`
* `_rollback()`

and two optional ones:

* `_setup()`
* `_teardown()`

Method signatures are the same for both sync and async clients, the only difference
is that async define them as coroutines.

### Sending

`_send()` method must take two arguments:

* `payload` of type `bytes` which is a serialized task payload to send. If
  transport expects text data, encoding must be done here.
* `meta` which is a Python `Mapping` with metadata. Metadata may contain
  only `str` keys and `str`, `bytes`, `int`, `float` or `bool` values. Metadata
  are not part of payload but can be used to pass additional control information.
  Some transports may support metadata for delivery retries or payload routing.
* `timeout` send timeout, when reached method should rise standard
  Python `TimeoutError` exception. Value might be any positive number
  which means seconds or `None` which means no timeout. `0` should be also
  allowed and means *immediately* which can be considered as
  underlying transport default timeouts.

!!! warning
    Make sure that raised exception is standard `TimeoutError`, many libraries
    rises their own `TimeoutError` exceptions which are not derived from standard
    one. In such case catch them and re-raise using standard exception.
  
    Only standard exception is caught and handled properly by Patchwork nodes.

!!! danger
    `asyncio.TimeoutError` exception raised by Python asynchronous utils like 
    `asyncio.wait()`are **not** derived from standard `TimeoutError` exception 
    and therefore must be caught and re-raised as standard exception!

It's not expected to return value from this method, but when returned
it's guaranteed that message has been delivered and transport accepted it.

!!! hint
    If transport provides ACK mechanism `_send()` method should wait for 
    confirmation. If there is several ACK levels it's a good practice to
    expose them as client configurable option.

### Fetching

`_fetch_one()` method takes one argument:

* `timeout` which is a waiting time for incoming message. Any positive number
  is a waiting time in seconds, `None` means no timeout (wait forever), `0` means
  no wait so method should return if there is pending message and not wait.
  
  If there will be no message to return in given time method should raise standard
  Python `TimeoutError` exception.
  
!!! warning
    Make sure that raised exception is standard `TimeoutError`, many libraries
    rises their own `TimeoutError` exceptions which are not derived from standard
    one. In such case catch them and re-raise using standard exception.
  
    Only standard exception is caught and handled properly by Patchwork nodes.

!!! danger
    `asyncio.TimeoutError` exception raised by Python asynchronous utils like 
    `asyncio.wait()`are **not** derived from standard `TimeoutError` exception 
    and therefore must be caught and re-raised as standard exception!
    
Method should return a tuple of message payload (in `bytes`) and a mapping
of metadata. If transport does not support metadata empty map should be returned.  

!!! note
    Fetched message **should not** be removed from message broker queue. Task should
    be just marked as taken and wait for commit or rollback.
    
    See [Commit and rollback](#commit-and-rollback) chapter below.  

### Commit and rollback

Each received task **should not** be removed from the message broker queue immediately
after receiving it. Instead, client should mark task as taken to avoid taking it
by another node and wait for `commit()` or `rollback()` method call. When task
will be processed successfully `commit()` method is called with processed task
instance as an argument and task can be safely removed from the queue. 
Otherwise `rollback()` is called and task should be unmarked as taken and can be
handled by another (or the same) node.

!!! note
    Default client implementation does not force any type of task tracking. Your
    class should track tasks returned by `_fetch_one()` to match them in `commit()`
    method or use metadata to pass required information (eg queue name, 
    message offset or ID).

`_commit()` and `_rollback()` method takes following arguments:

* `task` which is a `Task` instance to commit/rollback
* `timeout` which is an operation timeout. Any positive number is a timeout
  in seconds, `None` means no timeout, `0` means *immediately* which should be considered
  as underlying transport default timeouts.

!!! warning
    Make sure that raised exception is standard `TimeoutError`, many libraries
    rises their own `TimeoutError` exceptions which are not derived from standard
    one. In such case catch them and re-raise using standard exception.
  
    Only standard exception is caught and handled properly by Patchwork nodes.

!!! danger
    `asyncio.TimeoutError` exception raised by Python asynchronous utils like 
    `asyncio.wait()`are **not** derived from standard `TimeoutError` exception 
    and therefore must be caught and re-raised as standard exception!
    
It't not expected to return value from this method, but when returns task must
be successfuly committed / rollback'd.

### Setting up and finalization

Most clients working with external message broker needs some preparation and
initialization at the begging of operation and probably also finalization.

Base client class provides two methods for such jobs:

* `_setup()` which is called on client `start()` and should prepare client
* `_teardown()` which is called on client `stop()` and should flush all buffers,
  disconnect and clean up
  
Both methods takes no arguments and should not return any value. All errors
should be reported using exceptions.

If client use some kind of buffering or lazy sending, commits or rollbacks,
when `_teardown()` returns everything must be flushed without data loss. If
client class is destroyed without `stop()` method call (and therefore `_teardown()`)
data loss is allowed.

### Logging

Base client classes provide `logger` property which expose standard Python
logger interface. It's a good practice to use this logger to report any
clients event as this logger follows Patchwork loggers naming convention and
produced logs are compatible with other Patchwork components logs.

## Example

Here is an example of client which works locally on top of 
[asyncio.Queue](https://docs.python.org/3/library/asyncio-queue.html).

!!! warning
    As this client is just an example and works on local queue, commit mechanism
    is simplified. In real clients fetching message and storing it internally
    is a very bad practice. Message must be left on message queue to make
    sure that won't be lost when code terminates unexpectedly.


```python
from patchwork.client import AsyncClient


class AsyncLocalClient(AsyncClient):

    __shared = {}

    def __init__(self, *, queue: asyncio.Queue = None, name: str, serializer):
        super().__init__(serializer=serializer, name=name)
        if queue is None:
            if 'queue' not in self.__shared:
                self.__shared['queue'] = asyncio.Queue()

            queue = self.__shared['queue']

        self._queue = weakref.ref(queue)
        self._uncommitted = set()

    @classmethod
    def get_queue(cls):
        return cls.__shared['queue']

    def __repr__(self):
        res = super().__repr__()
        return f"<{res[1:-2]}, queue={self.queue}]>"

    @property
    def queue(self) -> asyncio.Queue:
        queue = self._queue()
        if queue is None:
            raise ConnectionAbortedError("Local queue lost")

        return queue

    async def _start(self):
        self.logger.debug(f"Client listening on local queue {self.queue}")

    async def _stop(self):
        for task in self._uncommitted:
            await self.send(task, timeout=0)

        self.logger.debug(f"Client stopped with {self.queue.qsize()} messages left on the queue")

    async def _send(self, payload, metadata, timeout: float = None):
        try:
            if timeout == 0:
                self.queue.put_nowait((payload, metadata))
            else:
                await asyncio.wait_for(self.queue.put((payload, metadata)), timeout=timeout)
        except asyncio.TimeoutError:
            raise TimeoutError(f"send operation timeout, can't deliver in {timeout}s")

    async def _fetch_one(self, timeout: float = None):
        try:
            return await asyncio.wait_for(self.queue.get(), timeout=timeout)
        except asyncio.TimeoutError:
            raise TimeoutError(f"fetch operation timeout, no messages in {timeout}s")

    async def commit(self, task, timeout: float = None):
        self._uncommitted.remove(task)

    async def rollback(self, task, timeout: float = None):
        # put the task back to the queue
        await self.send(task, timeout)

    async def get(self, timeout: float = None):
        task = await super().get(timeout)
        self._uncommitted.add(task)
        self.queue.task_done()
        return task
```